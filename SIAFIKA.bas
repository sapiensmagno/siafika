Attribute VB_Name = "SIAFIKA"
'
''''''''''''''''''''''''''''''''''''''SIAFIKA''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''SIAFI KEEP ALIVE ''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''' Macro escrita em 16/01/2015 por Jo�o Henrique Botelho de Amorim
''''' Analista Legislativo - Senado Federal/Secretaria de Finan�as, Or�amento e Contabilidade
''''' E-mail: jhbamorim@yahoo.com
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Option Explicit
Public SIAFI_Alive As Boolean
Public UltimaAcaoSIAFIKA As Variant
Public HoraSIAFI As Variant
Public Col As Long
Public idle As Long
Public Idle_Max As Long
Public LastPosition As Long
Sub SIAFI_KeepAlive()
        PD_Begin
        UltimaAcaoSIAFIKA = 0
        'With Range("status")
        '    .Value = "SIAFI Keep Alive - EXECUTANDO..."
        '    .font.Color = vbGreen
        'End With
        'Range("TimeControl").font.Color = vbWhite
        SIAFI_Alive = True
        TimerProc
End Sub
Public Sub SIAFI_NotKeepAlive()
        'With Range("status")
        '    .Value = ""
        '    .font.Color = vbGreen
        'End With
        'Range("TimeControl").Value = ""
    SIAFI_Alive = False
    ThisWorkbook.Sheets("Monitor").Range("SIAFIKAStatus").Value = "desligado"
    PD_End_sem_End
End Sub
Private Sub TimerProc()
    
    On Error Resume Next

    If SIAFI_Alive Then
        
        ' Determina o tempo de espera at� enviar tecla
        'If IsNumeric(Range("IDLE_MAX").Value) Then
        '    Idle_Max = Val(Range("IDLE_MAX").Value)
        'Else
            Idle_Max = 200
        'End If
        
        ' Pega a hora da tela do SIAFI ou do SIASG
        If PegaTextoEm(1, 6, 1, 10) = "SIASG" Then
            HoraSIAFI = CDate(PegaTextoEm(2, 36, 2, 40))
        Else
            For Col = 3 To 38
                If PegaTextoEm(2, Col, 2, Col) = ":" Then
                    HoraSIAFI = CDate(PegaTextoEm(2, Col - 2, 2, Col + 2))
                    Exit For
                End If
            Next Col
        End If
        
        ' Determina o tempo de inatividade
        If UltimaAcaoSIAFIKA = 0 Then
            idle = DateDiff("s", HoraSIAFI, Time())
        ElseIf CDate(HoraSIAFI) > CDate(UltimaAcaoSIAFIKA) Then
            idle = DateDiff("s", HoraSIAFI, Time())
        Else
            idle = DateDiff("s", UltimaAcaoSIAFIKA, Time())
        End If
        
        ' Envia tecla se o tempo m�ximo de inatividade foi atingido
        If idle >= Idle_Max Then
            LastPosition = Hod_PosicaoDoCursor
            PD_EnviaTecla "<PF23>"
            Hod_MoveParaPosicao (LastPosition)
            
            ' Imprime as vari�veis na planilha "Monitor" a cada PF23
            With Worksheets("Monitor")
                .Cells(.UsedRange.Rows.Count + 2, 1).Value = Idle_Max
                .Cells(.UsedRange.Rows.Count + 1, 2).Value = idle
                .Cells(.UsedRange.Rows.Count + 1, 3).Value = HoraSIAFI
                .Cells(.UsedRange.Rows.Count + 1, 4).Value = UltimaAcaoSIAFIKA
                .Cells(.UsedRange.Rows.Count + 1, 5).Value = Time()
            End With
            
            'If CDate(HoraSIAFI) > CDate(UltimaAcaoSIAFIKA) And UltimaAcaoSIAFIKA <> 0 Then
            '    UltimaAcaoSIAFIKA = 0
            'Else
                UltimaAcaoSIAFIKA = Time()
            'End If
        End If
        
        'Range("TimeControl").Value = "�ltimo hor�rio na tela do SIAFI: " & HoraSIAFI & " �ltima a��o do SIAFIKA: " & UltimaAcaoSIAFIKA & " Hora do sistema: " & Time() & " Minutos em inatividade: " & idle
        ThisWorkbook.Sheets("Monitor").Range("SIAFIKAStatus").Value = "ligado"
        StartTimer
        
    End If
    
End Sub

Private Sub StartTimer()
    If SIAFI_Alive Then Application.OnTime Now + TimeValue("00:00:10"), "TimerProc"
    'MsgBox (HoraSIAFI & vbNewLine & "-----" & vbNewLine & UltimaAcaoSIAFIKA)
End Sub


Sub fechar()

    SIAFI_NotKeepAlive
    
    ThisWorkbook.Saved = True 'Desconsidera quaisquer mudan�as para n�o precisar salvar
    
    If Workbooks.Count = 1 Then Application.Quit
    
End Sub

Sub teste()

    With Worksheets("Monitor")
    
        MsgBox (.UsedRange.Address)
        
    End With

End Sub
