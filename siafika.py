# -*- coding:utf-8 -*-
import EHLLAPI
import time
import datetime
import _strptime # For compatibility with py2exe
import sys

class Siafika(object):
    idle_max = 120

    def __init__(self, locker_path):
        self.hod = EHLLAPI.Emulator()
        self.locker_path = locker_path

    def check_locker(func):
        def check(self):
            if not self.locker_path:
                return func(self)
            with open(self.locker_path, 'r') as f:
                f.seek(0)
                status = f.read(4)
                f.close()
                if not status == 'BUSY':
                    return func(self)
        return check 

    @check_locker
    def check_siafi(self):
        self.connect()
        siafi_time = self.hod.get_field(field_pos=95, field_len=5)
        siafi_time = datetime.datetime.strptime(siafi_time, "%H:%M")
        current_time = datetime.datetime.now()
        idle = current_time - siafi_time # SIAFI gives us only minutes, so subtract 60 and limit minimum to 0.
        idle = max(idle.seconds - 60, 0)
        print "Idle: ", idle
        if idle > self.idle_max:
            last_cursor = self.hod.get_cursor()
            self.hod.send_keys("<PF23>")
            self.hod.set_cursor(last_cursor)
            print 'SIAFIKA! ', current_time.strftime("%b %d %Y %H:%M:%S")
        self.disconnect()

    def connect(self):
        self.hod.connect()

    def disconnect(self):
        self.hod.disconnect()

    def main_loop(self):
        try:
            self.check_siafi()
        except EHLLAPI.EmulatorError:
            print "Erro na conexao com o SIAFI."

path =  ""
if len(sys.argv) == 2: # lock-file path must be informed as an argument
    path = sys.argv[1]

print 'SIAFIKA. Para parar pressione CTRL+C.'
app = Siafika(locker_path=path)
while True:
    app.main_loop()
    time.sleep(5)
